import React, { useState } from 'react'
import Home from './Home';
import Question1 from './Questions/Question1';
import Question2 from './Questions/Question2';
import Question3 from './Questions/Question3';
import Question4 from './Questions/Question4';
import Question5 from './Questions/Question5';
import Question6 from './Questions/Question6';
import Question7 from './Questions/Question7';
import Question8 from './Questions/Question8';
import Question9 from './Questions/Question9';
import Question10 from './Questions/Question10';
import Question11 from './Questions/Question11';
import Question12 from './Questions/Question12';
import Result from './Result';

const Header = ({ testStarted, setTestStarted, setRespuestas, respuestas }) => {

    const [pregunta1, setPregunta1] = useState(false);
    const [pregunta2, setPregunta2] = useState(false);
    const [pregunta3, setPregunta3] = useState(false);
    const [pregunta4, setPregunta4] = useState(false);
    const [pregunta5, setPregunta5] = useState(false);
    const [pregunta6, setPregunta6] = useState(false);
    const [pregunta7, setPregunta7] = useState(false);
    const [pregunta8, setPregunta8] = useState(false);
    const [pregunta9, setPregunta9] = useState(false);
    const [pregunta10, setPregunta10] = useState(false);
    const [pregunta11, setPregunta11] = useState(false);
    const [pregunta12, setPregunta12] = useState(false);

    const [puntajeTotal, setPuntajeTotal] = useState(0);
    const [resultado, setResultado] = useState(false);
    

    let content;

    if (!testStarted) {
        content = <Home pregunta1={pregunta1}
        setPregunta1={setPregunta1}
        setTestStarted={setTestStarted}
        />
    }

    if (pregunta1) {
        content = <Question1 
        pregunta2={pregunta2}
        setPregunta2={setPregunta2}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta2) {
        content = <Question2 
        pregunta3={pregunta3}
        setPregunta3={setPregunta3}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta3) {
        content = <Question3 
        pregunta4={pregunta4}
        setPregunta4={setPregunta4}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta4) {
        content = <Question4
        pregunta5={pregunta5}
        setPregunta5={setPregunta5}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta5) {
        content = <Question5 
        pregunta6={pregunta6}
        setPregunta6={setPregunta6}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta6) {
        content = <Question6 
        pregunta7={pregunta7}
        setPregunta7={setPregunta7}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta7) {
        content = <Question7 
        pregunta8={pregunta8}
        setPregunta8={setPregunta8}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta8) {
        content = <Question8 
        pregunta9={pregunta9}
        setPregunta9={setPregunta9}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta9) {
        content = <Question9 
        pregunta10={pregunta10}
        setPregunta10={setPregunta10}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta10) {
        content = <Question10 
        pregunta11={pregunta11}
        setPregunta11={setPregunta11}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta11) {
        content = <Question11 
        pregunta12={pregunta12}
        setPregunta12={setPregunta12}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        />
    }

    if (pregunta12) {
        content = <Question12
        resultado={resultado}
        setResultado={setResultado}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        setPuntajeTotal={setPuntajeTotal}
        />
    }


    if (resultado) {
        content = <Result
        resultado={resultado}
        setResultado={setResultado}
        respuestas={respuestas}
        setRespuestas={setRespuestas}
        setTestStarted={setTestStarted}
        setPuntajeTotal={setPuntajeTotal}
        puntajeTotal={puntajeTotal}
        setPregunta1={setPregunta1}
        setPregunta2={setPregunta2}
        setPregunta3={setPregunta3}
        setPregunta4={setPregunta4}
        setPregunta5={setPregunta5}
        setPregunta6={setPregunta6}
        setPregunta7={setPregunta7}
        setPregunta8={setPregunta8}
        setPregunta9={setPregunta9}
        setPregunta10={setPregunta10}
        setPregunta11={setPregunta11}
        setPregunta12={setPregunta12}
        />
    }

    return (
        <>
            <header className='bg-white py-5 flex justify-center'>
                <a><img src='/logo.webp' className='w-100' /></a>
            </header>

            {content}
        </>
    )
}

export default Header
