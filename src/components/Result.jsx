import React, { useState } from 'react'

const Result = ({ respuestas, setRespuestas, setPuntajeTotal, puntajeTotal, setResultado,
  setPregunta1, setPregunta2, setPregunta3, setPregunta4, setPregunta5, setPregunta6, setPregunta7, setPregunta8, setPregunta9, setPregunta10,
  setPregunta11, setPregunta12
}) => {


  // Recorre el array de objetos y suma los valores numéricos.
  let total = 0;
  let content;

  respuestas.forEach((item) => {
    // Obtén el valor numérico de la propiedad del objeto.
    const puntaje = parseInt(Object.values(item)[0], 10);
    // Verifica si el valor es un número válido antes de sumarlo.
    if (!isNaN(puntaje)) {
      total += puntaje;
      setPuntajeTotal(total)
    }
  });


  if (puntajeTotal >= 12 && puntajeTotal <= 28) {
    content = `De manera preliminar, no hemos identificado señales de que necesites una valoración psicológica. Sin embargo, siempre hay mucho que mejorar, así que no dejes de trabajarte a ti mism@, mejorar tus habilidades y priorizarte😃.`
  }

  if (puntajeTotal >= 29 && puntajeTotal <= 44) {
    content = `No requieres de una valoración psicológica con urgencia, pero hemos detectado signos de alerta de que las cosas no van del todo bien😕. Presta atención a tus síntomas y si estos empeoran, ponte en contacto con nosotr@s o con el centro de salud mental de tu preferencia🌱.`
  }

  if (puntajeTotal >= 45 && puntajeTotal <= 60) {
    content = `Presentas signos de requerir una valoración psicológica con urgencia, por favor, ponte en contacto con nosotr@s o con el centro de salud mental de tu preferencia lo más pronto posible. `
  }

  const handleReset = () => {
   setPregunta1(false);
   setPregunta2(false);
   setPregunta3(false);
   setPregunta4(false);
   setPregunta5(false);
   setPregunta6(false);
   setPregunta7(false);
   setPregunta8(false);
   setPregunta9(false);
   setPregunta10(false);
   setPregunta11(false);
   setPregunta12(false);

    setPuntajeTotal(0);
    setRespuestas([]);
    setResultado(false);
  }


  return (
    <>
      <div className="isolate bg-white px-6 py-2 sm:py-2 lg:px-2">
        <div className="absolute inset-x-0 top-[-10rem] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[-20rem]" aria-hidden="true">
          <div className="relative left-1/2 -z-10 aspect-[1155/678] w-[36.125rem] max-w-none -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#68CFCE] to-[#C3D863] opacity-30 sm:left-[calc(50%-40rem)] sm:w-[72.1875rem]"></div>
        </div>
        <div className="py-10 sm:py-10 text-center font-Montserrat">

          <div className="mx-auto max-w-7xl px-6 lg:px-8">
            <div className="mx-auto max-w-2xl lg:text-center">
              <h2 className="text-base font-semibold leading-7 text-indigo-600"></h2>
              <p className="mt-2 mb-10 text-4xl font-bold tracking-tight text-teal-600 sm:text-4xl">
                Resultado de tu test
              </p>
              <p className="mt-6 leading-8 text-gray-600 text-xl">
                {content}
              </p>
            </div>
          </div>
        </div>
        <div className="m-10 flex items-center w-50">
          <button type="button" onClick={handleReset} className="m-3 mt-0 block w-full rounded-md bg-teal-600 px-3.5 py-2.5 text-center text-sm font-semibold text-white shadow-sm hover:bg-teal-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Finalizar</button>
        </div>
        <div className="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]" aria-hidden="true">
          <div className="relative left-[calc(50%+3rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 bg-gradient-to-tr from-[#68CFCE] to-[#C3D863] opacity-30 sm:left-[calc(50%+36rem)] sm:w-[72.1875rem]"></div>
        </div>
      </div>
    </>
  )
}

export default Result
