import React, { useState } from 'react'

const Question2 = ({ setPregunta3, respuestas, setRespuestas }) => {

    const [respuesta2, setRespuesta2] = useState('');

    const handleNextQuestion = () => {
        const objetoRespuesta = {
            respuesta2
        }

        setRespuestas([...respuestas, objetoRespuesta]);

        setPregunta3(true);
    }
    return (
        <>
    <div className="isolate bg-white px-6 py-10 sm:py-38 lg:px-8">
       
               <div className="absolute inset-x-0 top-[-10rem] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[-20rem]" aria-hidden="true">
                   <div className="relative left-1/2 -z-10 aspect-[1155/678] w-[36.125rem] max-w-none -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#68CFCE] to-[#C3D863] opacity-30 sm:left-[calc(50%-40rem)] sm:w-[72.1875rem]"></div>
               </div>
               <div className="mx-auto max-w-2xl text-center mt-0">
                   <p className="mt-0 text-2xl font-Montserrat font-bold leading-8 text-gray-600">Me aislo cuando me siento bajonead@ y no me desahogo con nadie.</p>
               </div>
               <form action="#" method="POST" className="mx-auto mt-10 max-w-xl sm:mt-10 font-Montserrat text-lg">
                   <div className="flex items-center mb-4">
                       <input onChange={(e) => setRespuesta2(e.target.value)} id="answer1" type="radio" value="1" name="respuesta1" className="w-10 h-5 text-teal-600 bg-gray-100 border-teal-300 focus:ring-teal-500 dark:focus:ring-teal-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                       <label htmlFor="answer1" className="ml-2 text-md text-gray-600 dark:text-gray-600">Totalmente en desacuerdo</label>
                   </div>
                   <div className="flex items-center mb-4">
                       <input onChange={(e) => setRespuesta2(e.target.value)} id="answer2" type="radio" value="2" name="respuesta1" className="w-10 h-5 text-teal-600 bg-gray-100 border-teal-300 focus:ring-teal-500 dark:focus:ring-teal-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                       <label htmlFor="answer2" className="ml-2 text-md text-gray-600 dark:text-gray-600">En desacuerdo</label>
                   </div>
                   <div className="flex items-center mb-4">
                       <input onChange={(e) => setRespuesta2(e.target.value)} id="answer3" type="radio" value="3" name="respuesta1" className="w-10 h-5 text-teal-600 bg-gray-100 border-teal-300 focus:ring-teal-500 dark:focus:ring-teal-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                       <label htmlFor="answer3" className="ml-2 text-md text-gray-600 dark:text-gray-600">Ni de acuerdo, ni en desacuerdo</label>
                   </div>
                   <div className="flex items-center mb-4">
                       <input onChange={(e) => setRespuesta2(e.target.value)} id="answer4" type="radio" value="4" name="respuesta1" className="w-10 h-5 text-teal-600 bg-gray-100 border-teal-300 focus:ring-teal-500 dark:focus:ring-teal-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                       <label htmlFor="answer4" className="ml-2 text-md text-gray-600 dark:text-gray-600">De acuerdo</label>
                   </div>
                   <div className="flex items-center mb-4">
                       <input onChange={(e) => setRespuesta2(e.target.value)} id="answer5" type="radio" value="5" name="respuesta1" className="w-10 h-5 text-teal-600 bg-gray-100 border-teal-300 focus:ring-teal-500 dark:focus:ring-teal-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                       <label htmlFor="answer5" className="ml-2 text-md text-gray-600 dark:text-gray-600">Totalmente de acuerdo</label>
                   </div>
                   
                   <p className='text-sm text-center font-medium leading-8 text-gray-400 mt-0 mb-0'>Pregunta 2 de 12</p>
                   <div className="m-10 flex items-center">
                    
                       <button type="button" onClick={handleNextQuestion} className="m-3 mt-0 block w-full rounded-md bg-teal-600 px-3.5 py-2.5 text-center text-sm font-semibold text-white shadow-sm hover:bg-teal-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Siguiente</button>
                   </div>
               </form>
           </div>
   </>
    )
}

export default Question2
