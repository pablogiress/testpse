import React from 'react'

const Home = ({setPregunta1, setTestStarted}) => {

    const handleClickHome = () => {
        
        setPregunta1(true);
        setTestStarted(false);
    }
  return (
    <div className='font-monserrat'>
    <div className="relative isolate px-6 pt-0 lg:px-8">
      <div className="absolute  -top-40 transform-gpu overflow-hidden blur-3xl sm:-top-80" aria-hidden="true">
        <div className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#68CFCE] to-[#C3D863] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"></div>
      </div>
      <div className="mx-auto max-w-2xl py-18 sm:py-18 lg:py-18">
        <div className="text-center">
          <h1 className="text-4xl mt-2 mb-20 font-bold font-Montserrat sm:text-6xl text-teal-700">Test de Salud Mental</h1>
          <p className="font-Montserrat text-lg">
            El siguiente formulario <b>NO</b> es una prueba diagnóstica, solo ayuda a identificar signos de que se requiere una valoración psicológica.<br />

            Lee cuidadosamente las siguientes preguntas y selecciona las opciones de respuesta que más se adecúen a tu situación actual.
          </p>
          <div className="mt-28 flex items-center justify-center gap-x-6">
          <a href="#" onClick={(e) => handleClickHome()} className="rounded-md bg-teal-500 px-10 py-3 text-md font-semibold text-white shadow-sm hover:bg-teal-600 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-teal-500">
              Iniciar
              </a>
          </div>
        </div>
      </div>
      <div className="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]" aria-hidden="true">
        <div className="relative left-[calc(50%+3rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 bg-gradient-to-tr from-[#68CFCE] to-[#C3D863] opacity-30 sm:left-[calc(50%+36rem)] sm:w-[72.1875rem]"></div>
      </div>
    </div>

  </div>
  )
}

export default Home
