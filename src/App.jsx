import { useState } from 'react'

import Home from './components/Home'
import Header from './components/Header'

function App() {

  const [testStarted, setTestStarted] = useState(false)
  const [respuestas, setRespuestas] = useState([]);

  return (
    <>
    <Header
    testStarted={testStarted}
    setTestStarted={setTestStarted}
    respuestas = {respuestas}
    setRespuestas = {setRespuestas}
    />
    </>
  )
}

export default App
